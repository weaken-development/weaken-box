package life.weaken.box;

import life.weaken.box.shulker.ShulkerCommand;
import life.weaken.box.shulker.ShulkerListener;
import life.weaken.box.shulker.ShulkerManager;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class BoxPlugin extends JavaPlugin {

    private ShulkerManager shulkerManager;

    @Override
    public void onEnable() {
        shulkerManager = new ShulkerManager(this);
        Bukkit.getPluginManager().registerEvents(new ShulkerListener(shulkerManager), this);
        registerCommands();
    }

    @Override
    public void onDisable() {
        shulkerManager.shutdown();
    }

    @SuppressWarnings("all")
    public void registerCommands() {
        getCommand("shulker").setExecutor(new ShulkerCommand(shulkerManager));
    }
}
